function ShoesList(props) {
  const handleDelete = (id) => {
    fetch("http://localhost:8080/api/shoes/" + id, { method: "DELETE" });
  };
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>id</th>
          <th>Name</th>
          <th>Color</th>
          <th>Manufacturer</th>
          <th>Image</th>
        </tr>
      </thead>
      <tbody>
        {props.shoe?.map((shoes) => {
          return (
            <tr key={shoes.id}>
              <td>{shoes.id}</td>
              <td>{shoes.model_name}</td>
              <td>{shoes.color}</td>
              <td>{shoes.manufacturer}</td>
              <td>{shoes.image}</td>
              <td>
                <button onClick={() => handleDelete(shoes.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
