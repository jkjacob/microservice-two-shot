function HatsList(props) {
  const handleDelete = (id) => {
    fetch("http://localhost:8090/api/hats/" + id, { method: "DELETE" });
  };

  return (
    <table>
      <thead>
        <tr>
          <th>Style Name</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Location</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {props.hats?.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.style_name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td>{hat.location}</td>
              <td>{hat.picture_url}</td>
              <td>
                <button onClick={() => handleDelete(hat.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>{" "}
      */}
    </table>
  );
}

export default HatsList;
