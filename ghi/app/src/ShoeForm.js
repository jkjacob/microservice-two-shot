import React, { useEffect, useState } from "react";

function ShoeCreateForm() {
  const [model_name, setModelName] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [color, setColor] = useState("");
  const [bin, setBin] = useState("");
  const [shoes, setShoes] = useState([]);
  const [image, setImage] = useState("");

  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.model_name = model_name;
    data.manufacturer = manufacturer;
    data.color = color;
    data.bin = bin;
    data.image = image;

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const shoeResponse = await fetch(shoeUrl, fetchOptions);
    if (shoeResponse.ok) {
      setModelName("");
      setManufacturer("");
      setColor("");
      setBin("");
      setImage("");
    }
  };

  const handleChangeName = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handleChangeManufacturer = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const handleChangeColor = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleChangeBin = (event) => {
    const value = event.target.value;
    setBin(value);
  };

  const handleChangeImage = (event) => {
    const value = event.target.value;
    setImage(value);
  };
  console.log(shoes);
  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="create-shoe-form">
                <h1 className="card-title">Create Shoe Form</h1>
                <div className="mb-3">
                  <select
                    onChange={handleChangeName}
                    name="shoe_name"
                    id="shoe_name"
                    required
                    className="form-control"
                  >
                    <option value="">Select shoe type </option>
                    {shoes.map((shoes) => {
                      return (
                        <option key={shoes.id} value={shoes.id}>
                          {shoes.model_name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <p className="mb-3">Input shoe details:</p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleChangeName}
                        required
                        placeholder="model_name"
                        type="text"
                        id="model_name"
                        name="model_name"
                        className="form-control"
                      />
                      <label htmlFor="manufacturer">Name:</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleChangeManufacturer}
                        required
                        placeholder="Manufacturer"
                        type="text"
                        id="manufacturer"
                        name="manufacturer"
                        className="form-control"
                      />
                      <label htmlFor="manufacturer">Manufacturer:</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleChangeColor}
                        required
                        placeholder="Input Color"
                        type="text"
                        id="color"
                        name="color"
                        className="form-control"
                      />
                      <label htmlFor="color">Color:</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleChangeBin}
                        required
                        placeholder="choose a bin"
                        type="number"
                        min="0"
                        id="bin_number"
                        name="bin_number"
                        className="form-control"
                      />
                      <label htmlFor="bin_number">Bin#:</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleChangeImage}
                        required
                        placeholder="input image url"
                        type="text"
                        id="image"
                        name="image"
                        className="form-control"
                      />
                      <label htmlFor="image">Image url: </label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ShoeCreateForm;
